
public class ContaCorrente {
	
	private String nome;
	private int CPF;
	protected double saldo;
	private double saldoInicial;

	public ContaCorrente(String nome, double saldo_inicial) {
		this.nome = nome;
		saldo = saldo_inicial;
	}
	
	public ContaCorrente(int CPF, double saldo_inicial) {
		this.CPF = CPF;
		saldo = saldoInicial;
	}
	

	public void depositar(double valor) {
		saldo += valor;
		System.out.println(" Valor depositado: " + valor);
		System.out.println("Novo saldo: " + saldo + "\n");
	}

	public void sacar(double valor) {
		saldo -= valor;
		System.out.println(" Valor sacado: " + valor);
		System.out.println("Novo valor de saldo: " + saldo + "\n");
	}

	public void saldo() {
		System.out.println("Nome: " + this.nome);
		System.out.printf(" Valor do saldo atual: %.2f\n", this.saldo);
	}


}
